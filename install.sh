#!/bin/bash

echo "Installing image and gpio libraries"
sudo apt install python3-picamera python3-dev python3-rpi.gpio imagemagick jhead exiftool -y

# echo "Configuring apache server"
# sudo apt install apache2
# sudo chown -R pi:www-data /var/www/html
# sudo chmod -R 770 /var/www/html
